package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	
	@FindBy(id = "viewLead_firstName_sp") WebElement eleFrstName;
	public ViewLeadPage()
	{
		PageFactory.initElements(driver, this);

	}
	
	public void verifyLeadFrstName(String expectedText)
	
	{
		verifyExactText(eleFrstName, expectedText);
	}
}
