package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);

	}
	
	@FindBy(how = How.XPATH, using = "//input[@name='firstName'][@class='inputBox']")
	WebElement eleLeadFrstName;
	
	@FindBy(how = How.XPATH, using = "//input[@name='lastName'][@class='inputBox']")
	WebElement eleLastName;
	
	@FindBy(how = How.XPATH, using = "//input[@name='companyName'][@class='inputBox']")
	WebElement eleCompName;
	
	@FindBy(how = How.XPATH, using = "//input[@value='Create Lead']")
	WebElement eleCreateLeadButton;
	
	
	public CreateLeadPage enterLeadFrstName(String fnm)
	{
		
		type(eleLeadFrstName, fnm);
		return this;
	}
	

	public CreateLeadPage enterLeadLastName(String lnm)
	{
		
		type(eleLastName, lnm);
		return this;
	}

	public CreateLeadPage enterLeadCompName(String cmpnm)
	{
		
		type(eleCompName, cmpnm);
		return this;
	}
	public ViewLeadPage clickOnLeadCompName()
	{
		
		click(eleCreateLeadButton);
		return new ViewLeadPage();
	}
	
	
	}

