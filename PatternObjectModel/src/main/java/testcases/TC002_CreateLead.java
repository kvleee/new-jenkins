package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLEad";
		authors = "Kanaga";
		category = "smoke";
		dataSheetName = "TC002_CreateLead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password,String fnm, String lnm, String cmpnm) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickOnCRMSFA()
		.clickOnLeads()
		.clickCreateLead()
		.enterLeadFrstName(fnm)
		.enterLeadLastName(lnm)
		.enterLeadCompName(cmpnm)
		.clickOnLeadCompName()
		;
				
	
	}
	
}
