package Tests;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
  public ChromeDriver driver;
  
	@Given ("Open the browser")
	 public void openBrowser() {
		System.setProperty("webdriver.chrome.driver","./drivers\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	@Given("Max the Browser")
	public void maxTheBrowser() {
	 
		driver.manage().window().maximize();
	}

	@Given("Set the Timout")
	public void setTheTimout() {
	    
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Given("Launch the URL")
	public void launchTheURL() {
	    
		driver.get("http://leaftaps.com/opentaps");
	}

	@Given("Enter the Username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String username) {
	
		driver.findElementById("username").sendKeys(username);
	}

	@Given("Enter the Password as (.*)")
	public void enterThePasswordAsCrmsfa(String pwd) {
	   
		driver.findElementById("password").sendKeys(pwd);
	}

	@Given("Click on the Login Button")
	public void clickOnTheLoginButton() {
	
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("Click On CRM\\/SFA")
	public void clickOnCRMSFA() {
	    
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("Click on the Leads")
	public void clickOnTheLeads() {
		
		driver.findElementByXPath("//a[.='Leads']").click();

	}

	@Given("Click on the Create Lead")
	public void clickOnTheCreateLead() {

		driver.findElementByXPath("//a[.='Create Lead']").click();
	}

	@Given("Enter the CompanyName (.*)")
	public void enterTheCompanyNameAsInfosys(String CompName) {
	    
		driver.findElementByXPath("//input[@name='companyName'][@class='inputBox']").sendKeys(CompName);
	}

	@Given("Enter the FirstName (.*)")
	public void enterTheFirstNameAsKanagavalli(String FrstName) {
	    
		driver.findElementByXPath("//input[@name='firstName'][@class='inputBox']").sendKeys(FrstName);
	}

	@Given("Enter the LastName (.*)")
	public void enterTheLastNameAsGobinath(String LastName) {
	  
		driver.findElementByXPath("//input[@name='lastName'][@class='inputBox']").sendKeys(LastName);
	}

	@When("Click on Create Lead Button")
	public void clickOnCreateLeadButton() {
	   driver.findElementByXPath("//input[@value='Create Lead']").click();
	}

	@Then("Verify the Firstname")
	public void verifyTheFirstname() {
		System.out.println("Success");
	  
	
	}
		@Then("Verify the failure")
		public void verifyFailure() {
			System.out.println("Failure");
		}
}