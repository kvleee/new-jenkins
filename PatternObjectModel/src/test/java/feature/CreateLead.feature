Feature: Create Lead in Leaftaps

#Background:
#
#Given Open the browser
#And Max the Browser
#And Set the Timout
#And Launch the URL
#And Enter the Username as DemoSalesManager
#And Enter the Password as crmsfa
#And Click on the Login Button

Scenario Outline: Create Login
Given Click On CRM/SFA
#Then Verify the Login    
And Click on the Leads
And Click on the Create Lead
And Enter the CompanyName <compName>
And Enter the FirstName <frstName>
And Enter the LastName <lastName>
When Click on Create Lead Button
Then Verify the Firstname

Examples:
|compName|frstName|lastName|
|Infy|Kvl|EEE|
|TCS|Kags|Gobi|


Scenario: CreateLead Fail
Given Click On CRM/SFA
#Then Verify the Login    
And Click on the Leads
And Click on the Create Lead
And Enter the CompanyName <compName1>
And Enter the FirstName <frstName2>
And Enter the LastName <lastName2>
When Click on Create Lead Button
Then Verify the failure  

#Examples:
#|compName1|frstName2|lastName2|
#|Fake||Faking|
